import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

public class GoeuroTest {

	public String cityName() {
		//input city name
		Scanner input = new Scanner(System.in);
		System.out.print("Enter name of city: ");
		String cityname = input.next();
		System.out.println("city name is"+cityname);
		return cityname;

	}

	public void fileWrite(String cityname) {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		JsonParser jp = new JsonParser();


		try {

			URL url = new URL("http://api.goeuro.com/api/v2/position/suggest/en/"+cityname);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();

			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");

			//check the response code
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader(
					(conn.getInputStream())));


			String output; 
			
			System.out.println("Output from Server is \n");
			while ((output = br.readLine()) != null) {
				System.out.println(output);
				JsonElement je = jp.parse(output);
				String json = gson.toJson(je);

				FileWriter fileWriter = null;	

				try {

					fileWriter = new FileWriter("CSVFILE.csv");  

					fileWriter.append(json); 

					System.out.println("CSV file was created successfully !!!");



				} catch (Exception e) {

					System.out.println("Error occured while CsvFileWriting");

					e.printStackTrace();

				} finally {



					try {

						fileWriter.flush();

						fileWriter.close();

					} catch (IOException e) {

						System.out.println("error occured while flushing/closing fileWriter");

						e.printStackTrace();

					}



				}
			}
			conn.disconnect();

		} catch (IOException e) {

			e.printStackTrace();

		} 


	}

	public static void main(String[] args) {

		GoeuroTest g = new GoeuroTest();
		String nmofcity=g.cityName();


		g.fileWrite(nmofcity);





	}

}
